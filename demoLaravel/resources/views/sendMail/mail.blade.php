<style type="text/css">
	#cart_items .cart_info {
	    border: 1px solid #E6E4DF;
	    margin-bottom: 50px;
	}
	#cart_items .cart_info .table.table-condensed tr:last-child {
	    border-bottom: 0;
	}
	#cart_items .cart_info .table.table-condensed thead tr {
	    height: 51px;
	}
	#cart_items .cart_info .table.table-condensed tr {
	    border-bottom: 1px solid#F7F7F0;
	}
	#cart_items .cart_info .cart_menu {
	    background: #FE980F;
	    color: #fff;
	    font-size: 16px;
	    font-family: 'Roboto', sans-serif;
	    font-weight: normal;
	}
	#cart_items .cart_info .image {
	    padding-left: 30px;
	}
	.cart_info table tr td {
	    border-top: 0 none;
	    vertical-align: inherit;
	    margin-right: 5px;
	}
</style>
<div class="table-responsive cart_info tb-cart">
	{{$slug}}
</div>