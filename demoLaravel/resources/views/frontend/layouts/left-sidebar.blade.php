<script type="text/javascript">
    $(document).ready(function(){
    	$('.slider-track').click(function(e){
            var getPrice = $('.tooltip-inner').text();
        	//console.log(getPrice);

        	$.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: 'post',
                url: '/search/price',
                data: {'price' : getPrice},
                success: function (data) {
                    //console.log(data);
                    if (data.length > 0) {
                    	var html = '';
                    	data.forEach(function (product) {
                    		var arrImage = JSON.parse(product['image']);
                    		var image = "{{asset('upload/product/')}}"+ "/"+ product['user_id'] +"/"+ arrImage[0];
                    		var url = "{{url('product/detail/')}}" + "/" + product['id'];
                    		html += '<div class="col-sm-4">'+
								'<div class="product-image-wrapper">'+
									'<div class="single-products">'+
										'<div class="productinfo text-center">'+
											'<a href="'+url+'"><img class="" src="'+image+'" alt="" /></a>'+
											'<h2>'+product['price']+'đ</h2>'+
											'<a href="'+url+'"><p>'+product['name']+'</p></a>'+
											'<a id="'+product['id']+'" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>'+
										'</div>'+
									'</div>'+
								'</div>'+
							'</div>';
	                    });
	                    $('.features_items').empty();
	                    $('.features_items').append('<h2 class="title text-center">Search Results</h2>');
	                    $('.features_items').append(html);
                    }
                    else {
                    	$('.features_items').empty();
                    	$('.features_items').append('<h2 class="title text-center">Search Results</h2>');
                    	$('.features_items').append('<h4 style="text-align: center; color: gray">Không tìm thấy sản phẩm</h4>');
                    }
                }
            });
    	});
    });
</script>

<div  class="col-sm-3 left-screen">
	<div class="left-sidebar">
		<h2>Category</h2>
		<div class="panel-group category-products" id="accordian"><!--category-productsr-->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
							<span class="badge pull-right"><i class="fa fa-plus"></i></span>
							Sportswear
						</a>
					</h4>
				</div>
				<div id="sportswear" class="panel-collapse collapse">
					<div class="panel-body">
						<ul>
							<li><a href="#">Nike </a></li>
							<li><a href="#">Under Armour </a></li>
							<li><a href="#">Adidas </a></li>
							<li><a href="#">Puma</a></li>
							<li><a href="#">ASICS </a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordian" href="#mens">
							<span class="badge pull-right"><i class="fa fa-plus"></i></span>
							Mens
						</a>
					</h4>
				</div>
				<div id="mens" class="panel-collapse collapse">
					<div class="panel-body">
						<ul>
							<li><a href="#">Fendi</a></li>
							<li><a href="#">Guess</a></li>
							<li><a href="#">Valentino</a></li>
							<li><a href="#">Dior</a></li>
							<li><a href="#">Versace</a></li>
							<li><a href="#">Armani</a></li>
							<li><a href="#">Prada</a></li>
							<li><a href="#">Dolce and Gabbana</a></li>
							<li><a href="#">Chanel</a></li>
							<li><a href="#">Gucci</a></li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordian" href="#womens">
							<span class="badge pull-right"><i class="fa fa-plus"></i></span>
							Womens
						</a>
					</h4>
				</div>
				<div id="womens" class="panel-collapse collapse">
					<div class="panel-body">
						<ul>
							<li><a href="#">Fendi</a></li>
							<li><a href="#">Guess</a></li>
							<li><a href="#">Valentino</a></li>
							<li><a href="#">Dior</a></li>
							<li><a href="#">Versace</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"><a href="#">Kids</a></h4>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"><a href="#">Fashion</a></h4>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"><a href="#">Households</a></h4>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"><a href="#">Interiors</a></h4>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"><a href="#">Clothing</a></h4>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"><a href="#">Bags</a></h4>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"><a href="#">Shoes</a></h4>
				</div>
			</div>
		</div><!--/category-products-->
	
		<div class="brands_products"><!--brands_products-->
			<h2>Brands</h2>
			<div class="brands-name">
				<ul class="nav nav-pills nav-stacked">
					<li><a href="#"> <span class="pull-right">(50)</span>Acne</a></li>
					<li><a href="#"> <span class="pull-right">(56)</span>Grüne Erde</a></li>
					<li><a href="#"> <span class="pull-right">(27)</span>Albiro</a></li>
					<li><a href="#"> <span class="pull-right">(32)</span>Ronhill</a></li>
					<li><a href="#"> <span class="pull-right">(5)</span>Oddmolly</a></li>
					<li><a href="#"> <span class="pull-right">(9)</span>Boudestijn</a></li>
					<li><a href="#"> <span class="pull-right">(4)</span>Rösch creative culture</a></li>
				</ul>
			</div>
		</div><!--/brands_products-->
		
		<div class="price-range"><!--price-range-->
			<h2>Price Range</h2>
			<div  class="well text-center">
				 	<input type="text" className="span2" data-slider-min={0} data-slider-max={250000} data-slider-step={10000} data-slider-value="[30000,90000]" id="sl2" /><br />
  					<b className="pull-left">0 đ</b> <b className="pull-right">250000 đ</b>
			</div>
		</div><!--/price-range-->
		
		<div class="shipping text-center"><!--shipping-->
			<img src="{{ asset('frontend/images/home/shipping.jpg') }}" alt="" />
		</div><!--/shipping-->
	
	</div>
</div>