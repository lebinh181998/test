<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Home | E-Shopper</title>
    <link href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/prettyPhoto.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/price-range.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/animate.css') }}" rel="stylesheet">
	<link href="{{ asset('frontend/css/main.css') }}" rel="stylesheet">
	<link href="{{ asset('frontend/css/responsive.css') }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('frontend/images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('frontend/images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('frontend/images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('frontend/images/ico/apple-touch-icon-57-precomposed.png') }}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link type="text/css" rel="stylesheet" href="{{ asset('frontend/css/rate.css')}}">

    <!-- <style type="text/css">
        .frame:hover {
          transform: scale(1.5);
          position: relative;
          z-index: 2;
        }
    </style> -->
</head><!--/head-->

<body>
    @include('frontend.layouts.header')
    <section>
        <div class="container">
            <div class="row">
                @include('frontend.layouts.left-sidebar')
                @yield('content')
            </div>
        </div>
    </section>
    @include('frontend.layouts.footer')

    <script src="{{ asset('frontend/js/jquery.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.scrollUp.min.js') }}"></script>
    <script src="{{ asset('frontend/js/price-range.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('frontend/js/main.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("a[rel^='prettyPhoto']").prettyPhoto();
        });
    </script>
    
    <script type="text/javascript">
        $(document).ready(function(){
            $('.add-to-cart').click(function(e){
                e.preventDefault();
                var getId = $(this).attr('id');

                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    method: 'post',
                    url: '/ajax-addtocart',
                    data: {'id' : getId},
                    success: function (data) {
                        alert('Bạn đã thêm sản phẩm thành công');
                    }
                });
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.up').click(function(e){
                e.preventDefault();
                var id = $(this).attr('id');
                var pricepro = $('#price' + id).text();
                var sumpricepro = $('.sumpricepro').text();

                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    method: 'post',
                    url: '/ajax-cart/flus',
                    data: {'id' : id},
                    success: function(up){
                        $('#amount' + id).val(up);
                        $('#sumprice' + id).text(parseInt(pricepro)*up);
                        $('.sumpricepro').text(parseInt(sumpricepro) + parseInt(pricepro));
                    }
                });
            });

            $('.down').click(function(e){
                e.preventDefault();
                var id = $(this).attr('id');
                var pricepro = $('#price' + id).text();
                var sumpricepro = $('.sumpricepro').text();

                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    method: 'post',
                    url: '/ajax-cart/minus',
                    data: {'id' : id},
                    success: function(down){
                        if (down >= 1) {
                            $('#amount' + id).val(down);
                            $('#sumprice' + id).text(parseInt(pricepro)*down);
                            $('.sumpricepro').text(parseInt(sumpricepro) - parseInt(pricepro));
                        }
                    }
                });
            });

            $('.delete').click(function(e){
                e.preventDefault();
                var id = $(this).attr('id');
                var sumpricepro = $('.sumpricepro').text();
                var sumprice = $('#sumprice' + id).text();

                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    method: 'post',
                    url: '/ajax-cart/delete',
                    data: {'id' : id},
                    success: function(texthtml){
                        $('tr.'+ id).remove();
                        $('#count').text(parseInt($('#count').text()) - 1);
                        $('.sumpricepro').text(parseInt(sumpricepro) - parseInt(sumprice));
                    }
                });
            });
        });
    </script>
</body>
</html>