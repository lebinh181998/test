@extends('frontend.layouts.apps')
@section('content')
<style type="text/css">

	ul.menu-search li{
		margin-bottom: 50px;
		display: inline-block;
		width: 17%;
	}
	ul.menu-search li input {

    border: 1px solid #F0F0E9;
    color: #B2B2B2;
    font-family: 'roboto';
    font-size: 12px;
    font-weight: 300;
    height: 35px;
    outline: medium none;
    padding-left: 10px;
    width: 100%;
    background-repeat: no-repeat;
    background-position: 130px;
}
	ul.menu-search li select{
		font-family: 'roboto';
	    font-size: 12px;
	    font-weight: 400;
	}
</style>
<script type="text/javascript">
	
</script>
<div class="col-sm-9 padding-right">
	<div>
		<ul class="menu-search">
			<form method="post" action="/search">
				@csrf
				<li>
					<input type="text" name="name" placeholder="Name">
				</li>
				<li>
					<select name="price" class="form-control form-control-line">
						<option value="">Chọn giá</option>
	                    <option value="100000">100000</option>
	                    <option value="500000">500000</option>
	                    <option value="1000000">1000000</option>
	                </select>
				</li>
				<li>
					<select name="category" class="form-control form-control-line">
						<option value="">Chọn thể loại</option>
                        @foreach($getCategory as $category)
                            <option value="{{$category['id']}}">{{ $category['name'] }}</option>
                        @endforeach
                    </select>
				</li>
				<li>
					<select name="brand" class="form-control form-control-line">
						<option value="">Chọn nhãn hàng</option>
                        @foreach($getBrand as $brand)
                            <option value="{{$brand['id']}}">{{ $brand['name'] }}</option>
                        @endforeach
                    </select>
				</li>
				<li>
					<select id="status" name="status" class="form-control form-control-line">
						<option value="">Chọn trạng thái</option>
                        @foreach($getSale as $sale)
                            <option value="{{$sale['status']}}">{{ $sale['name'] }}</option>
                        @endforeach
                    </select>
				</li>

				<input style="margin-right: 15px; background-color: #FE980F; color: #ffffff" class="btn btn-default pull-right" type="submit" name="search" value="Search">
			</form>
			
		</ul>
	</div>
	<div class="features_items"><!--features_items-->
		<h2 class="title text-center">Search Results</h2>
		@if(!empty($getProduct))
			@foreach($getProduct as $product)
			<div class="col-sm-4">
				<div class="product-image-wrapper">
					<div class="single-products">
						<div class="productinfo text-center">
							<a href="{{url('product/detail/'.$product->id)}}"><img class="" src="{{asset('upload/product/'.$product->user_id.'/'.$getImage[$product->id][0])}}" alt="" /></a>

							<h2>{{$product->price}}đ</h2>
							<a href="{{url('product/detail/'.$product->id)}}"><p>{{$product->name}}</p></a>
							<a id="{{$product->id}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		@else
			<h4 style="text-align: center; color: gray">Không tìm thấy sản phẩm</h4>
		@endif
	</div>
</div>
@endsection