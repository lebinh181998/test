@extends('frontend.layouts.apps')
@section('content')
<style type="text/css">
	.left-screen{
		display: none;
	}
</style>
<script type="text/javascript">
	$(document).ready(function(){
		$('#thanhtoan').click(function(){
			$authCheck = '{{Auth::check()}}';
			if (!$authCheck) {
				alert('Vui lòng đăng nhập để thanh toán');
				return false;
			}
			else {
				var html = $('.tb-cart').html();
				// console.log(html);
				var price_sum = $('.sumtotal').text();

				$.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    method: 'post',
                    url: '/checkout',
                    data: {'html' : html, 'price_sum' : price_sum},
                    success: function (data) {
                    	if (data > 0) {
                    		alert('Thanh toán thành công');
                    		window.location.replace("/index");
                    	}
                        else {
                        	alert('Không có sản phẩm để thanh toán');
                        	return false;
                        }
                        
                    },
                    error: function (data) {
                    	alert(data);
                    	return false;
                    },
                });
			}
		});
	});
</script>
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Check out</li>
				</ol>
			</div><!--/breadcrums-->

			@if(!Auth::check())
			<div class="shopper-informations">
				<div class="row">
					<div class="col-sm-3 col-sm-ofset-9">
						<div class="shopper-info">
							<p>Login</p>
							<form method="post" action="/member/login">
								@csrf
								<input type="email" name="email" placeholder="Email">
								<input type="password" name="password" placeholder="Password">
								<button type="submit" name="login" class="btn btn-primary" href="">Login</button>
							</form>
						</div>
					</div>				
				</div>
			</div>
			@endif
			<div class="review-payment">
				<h2>Review & Payment</h2>
			</div>

			<div class="table-responsive cart_info tb-cart">
				<table style="text-align: center;" class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody id="showcart" class="">
						@if(!empty($getProduct))
							@foreach($getProduct as $product)
								<tr class="{{$product['id']}}">
									<td class="cart_product">
										<a href=""><img class="img" src="{{ asset('upload/product/'.$product['user_id'].'/2'.$getImage[$product['id']][0]) }}" alt=""></a>
									</td>
									<td class="cart_description">
										<p style="font-size: 20px;">{{$product['name']}}</p>
									</td>
									<td class="cart_price">
										<p id="price{{$product['id']}}" class="Price">{{$product['price']}}</p>
									</td>
									<td style="display: inline-block;" class="cart_quantity">
										<div class="cart_quantity_button">
											<a id="{{$product['id']}}" class="cart_quantity_up up" href=""> + </a>
											<input id="amount{{$product['id']}}" class="cart_quantity_input Cart" 
											type="text" name="quantity" 
											value="{{$arrProductQTY[$product['id']]}}" autocomplete="off" size="2">
											<a id="{{$product['id']}}" class="cart_quantity_down down" href=""> - </a>
										</div>
									</td>
									<td class="cart_total">
										<p id="sumprice{{$product['id']}}" class="cart_total_price SUMprice">
											{{$product['price'] * $arrProductQTY[$product['id']]}}
										</p>
									</td>
									<td class="cart_delete">
										<a id="{{$product['id']}}" class="cart_quantity_delete delete" href=""><i class="fa fa-times"></i></a>
									</td>
								</tr>
							@endforeach
							<tr>
								<td colspan="4">&nbsp;</td>
								<td colspan="2">
									<table class="table table-condensed total-result">
										<tbody>
											<tr>
												<td>Cart Sub Total</td>
												<td><span style="font-weight: inherit; color: inherit;" class="sumpricepro">{{$sum}}</span></td>
											</tr>
											<tr>
												<td>Exo Tax</td>
												<td>0</td>
											</tr>
											<tr class="shipping-cost">
												<td>Shipping Cost</td>
												<td>Free</td>										
											</tr>
											<tr>
												<td>Total</td>
												<td><span style="font-weight: inherit; color: inherit;" class="sumtotal">{{$sum}}</span></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						@else
							<td colspan="5"><h2>Không có sản phẩm nào</h2></td>
						@endif
						
					</tbody>
				</table>
			</div>

			<div style="text-align: right;">
				<button type="submit" id="thanhtoan" name="thanhtoan" class="btn btn-primary">Thanh toán</button>
			</div>

			<div class="payment-options">
					<span>
						<label><input type="checkbox"> Direct Bank Transfer</label>
					</span>
					<span>
						<label><input type="checkbox"> Check Payment</label>
					</span>
					<span>
						<label><input type="checkbox"> Paypal</label>
					</span>
				</div>
		</div>
	</section> <!--/#cart_items-->
@endsection