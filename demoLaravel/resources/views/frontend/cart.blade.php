@extends('frontend.layouts.apps')
@section('content')
<style type="text/css">
	.left-screen{
		display: none;
	}
</style>
<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
			  <li><a href="#">Home</a></li>
			  <li class="active">Shopping Cart</li>
			</ol>
		</div>
		<div class="table-responsive cart_info">
			<table style="text-align: center;" class="table table-condensed">
				<thead>
					<tr class="cart_menu">
						<td class="image">Item</td>
						<td class="description">Name</td>
						<td class="price">Price</td>
						<td class="quantity">Quantity</td>
						<td class="total">Total</td>
						<td></td>
					</tr>
				</thead>
				<tbody id="showcart">
					@if(!empty($getProduct))
						@foreach($getProduct as $product)
							<tr class="{{$product['id']}}">
								<td class="cart_product">
									<a href=""><img class="img" src="{{ asset('upload/product/'.$product['user_id'].'/2'.$getImage[$product['id']][0]) }}" alt=""></a>
								</td>
								<td class="cart_description">
									<p style="font-size: 20px;">{{$product['name']}}</p>
								</td>
								<td class="cart_price">
									<p id="price{{$product['id']}}" class="Price">{{$product['price']}}</p>
								</td>
								<td style="display: inline-block;" class="cart_quantity">
									<div class="cart_quantity_button">
										<a id="{{$product['id']}}" class="cart_quantity_up up" href=""> + </a>
										<input id="amount{{$product['id']}}" class="cart_quantity_input Cart" 
										type="text" name="quantity" 
										value="{{$arrProductQTY[$product['id']]}}" autocomplete="off" size="2">
										<a id="{{$product['id']}}" class="cart_quantity_down down" href=""> - </a>
									</div>
								</td>
								<td class="cart_total">
									<p id="sumprice{{$product['id']}}" class="cart_total_price SUMprice">
										{{$product['price'] * $arrProductQTY[$product['id']]}}
									</p>
								</td>
								<td class="cart_delete">
									<a id="{{$product['id']}}" class="cart_quantity_delete delete" href=""><i class="fa fa-times"></i></a>
								</td>
							</tr>
						@endforeach
					@else
						<td colspan="5"><h2>Không có sản phẩm nào</h2></td>
					@endif
				</tbody>
				
			</table>
		</div>
	</div>
</section> <!--/#cart_items-->

<section id="do_action">
	<div class="container">
		<div class="heading">
			<h3>What would you like to do next?</h3>
			<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="chose_area">
					<ul class="user_option">
						<li>
							<input type="checkbox">
							<label>Use Coupon Code</label>
						</li>
						<li>
							<input type="checkbox">
							<label>Use Gift Voucher</label>
						</li>
						<li>
							<input type="checkbox">
							<label>Estimate Shipping & Taxes</label>
						</li>
					</ul>
					<ul class="user_info">
						<li class="single_field">
							<label>Country:</label>
							<select>
								<option>United States</option>
								<option>Bangladesh</option>
								<option>UK</option>
								<option>India</option>
								<option>Pakistan</option>
								<option>Ucrane</option>
								<option>Canada</option>
								<option>Dubai</option>
							</select>
							
						</li>
						<li class="single_field">
							<label>Region / State:</label>
							<select>
								<option>Select</option>
								<option>Dhaka</option>
								<option>London</option>
								<option>Dillih</option>
								<option>Lahore</option>
								<option>Alaska</option>
								<option>Canada</option>
								<option>Dubai</option>
							</select>
						
						</li>
						<li class="single_field zip-field">
							<label>Zip Code:</label>
							<input type="text">
						</li>
					</ul>
					<a class="btn btn-default update" href="">Get Quotes</a>
					<a class="btn btn-default check_out" href="">Continue</a>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="total_area">
					<ul>
						<li>Cart Sub Total <span class="sumpricepro">{{$sum}}</span></li>
						<li>Eco Tax <span>$2</span></li>
						<li>Shipping Cost <span>Free</span></li>
						<li>Total <span>$61</span></li>
					</ul>
					<form method="post" action="">
						<button type="submit" name="update" class="btn btn-default update">Update</button>
						<a type="submit" href="/checkout" name="checkout" class="btn btn-default check_out">Check Out</a>
					</form>
						
				</div>
			</div>
		</div>
	</div>
</section><!--/#do_action-->
@endsection