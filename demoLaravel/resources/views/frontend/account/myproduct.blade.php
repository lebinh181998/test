<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Home | E-Shopper</title>
    <link href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/prettyPhoto.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/price-range.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/animate.css') }}" rel="stylesheet">
	<link href="{{ asset('frontend/css/main.css') }}" rel="stylesheet">
	<link href="{{ asset('frontend/css/responsive.css') }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('frontend/images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('frontend/images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('frontend/images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('frontend/images/ico/apple-touch-icon-57-precomposed.png') }}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link type="text/css" rel="stylesheet" href="{{ asset('frontend/css/rate.css')}}">

    <script type="text/javascript">
        $(document).ready(function(){
            $('.delete').click(function(e){
                e.preventDefault();
                var getId = $(this).attr('id');

                $.ajaxSetup({
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                $.ajax({
                    method: 'post',
                    url: '/product/delete',
                    data: {'id' : getId},
                    success: function ($data) {
                        alert($data);
                    }
                });
            });
        });
    </script>
</head><!--/head-->

<body>
    @include('frontend.layouts.header')
    <section style="margin-bottom: 100px;">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a id="profile" href="{{url('account/profile')}}">Profile</a></h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a id="myproduct" href="{{url('account/myproduct')}}">My product</a></h4>
                                </div>
                            </div>
                        </div><!--/category-productsr-->
                    </div>
                </div>
                
                <div class="col-sm-9">

                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if(session('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                            {{session('success')}}
                        </div>
                    @endif

                    <!-- ---------------------- SHOW MY PRODUCT------------------- -->
					
					<div class="table-responsive cart_info myproduct">
						<h2 style="color: #FE980F;font-family: 'Roboto', sans-serif; font-size: 18px;font-weight: 700;" class="title text-center">MY PRODUCT</h2>
						<table style="text-align: center;" class="table table-condensed tbproduct">
							<thead>
								<tr class="cart_menu">
									<td class="image">Stt</td>
									<td class="image">Item</td>
									<td class="description">Name</td>
									<td class="price">Price</td>
									<td class="quantity">Edit</td>
									<td class="total">Delete</td>
									<td></td>
								</tr>
							</thead>
							<tbody id="showproduct">
								@foreach($getProduct as $product)
									<tr class="{{$product['id']}}">
										<td class="cart_description">
											{{$stt++}}
										</td>
										<td class="cart_product">
											<a href=""><img style="width: 121px; height: 86px;" class="imgpro" src="{{ asset('upload/product/'.$userId.'/2'.$getImage[$product['id']][0]) }}" alt=""></a>
										</td>
										<td class="cart_description">
											<h4><a href="">{{$product['name']}}</a></h4>
										</td>
									<td class="cart_price">
											<a class="Price">{{$product['price']}}</a>
										</td>
										<td class="cart_price">
											<a class="edit" href="{{url('product/edit/'.$product['id'])}}">Edit</a>
										</td>
					                	<td class="cart_delete">
					                		<a id="{{$product['id']}}" class="cart_quantity_delete delete" href=""><i class="fa fa-times"></i></a>
					                	</td>
									</tr>
								@endforeach
							</tbody>
						</table>
						<a style="float: right; margin-top: 20px;" class="btn btn-primary" type="submit" name="addproduct" href="{{url('product/add')}}">Add New</a>
					</div>
                </div>
            </div>
        </div>
    </section>
    @include('frontend.layouts.footer')

    <script src="{{ asset('frontend/js/jquery.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.scrollUp.min.js') }}"></script>
    <script src="{{ asset('frontend/js/price-range.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('frontend/js/main.js') }}"></script>
</body>
</html>