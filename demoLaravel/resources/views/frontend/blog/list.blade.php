@extends('frontend.layouts.apps')
@section('content')
	<div class="col-sm-9">
		<div class="blog-post-area">
			<h2 class="title text-center">Latest From our Blog</h2>
			@foreach($getBlog as $blog)
				<div class="single-blog-post">
					<h3>{{$blog['title']}}</h3>
					<div class="post-meta">
						<ul>
							<li><i class="fa fa-user"></i> {{$blog['user_name']}}</li>
							<li><i class="fa fa-clock-o"></i> {{$blog['updated_at']->toTimeString()}}</li>
							<li><i class="fa fa-calendar"></i> {{$blog['updated_at']->toDateString()}}</li>
						</ul>
						<span>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-half-o"></i>
						</span>
					</div>
					<a href="">
						<img style="height: 390px" src="{{ asset('upload/user/avatar/'.$blog['image']) }}" alt="">
					</a>
					<p>{{$blog['description']}}</p>
					<a  class="btn btn-primary" href="{{ url('blog/detail/'.$blog['id']) }}">Read More</a>
				</div>
			@endforeach
			<div class="col-sm-12" style="text-align: center; margin-top: 20px;">
				{{ $getBlog->links() }}
			</div>
		</div>
    </div>
@endsection