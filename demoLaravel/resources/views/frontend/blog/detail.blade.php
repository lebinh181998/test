@extends('frontend.layouts.apps')
@section('content')

<script type="text/javascript">
    if(screen.width <= 736){
        document.getElementById("viewport").setAttribute("content", "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no");
    }
</script>

<script type="text/javascript">
	$(document).ready(function(){
		//vote
		$('.ratings_stars').hover(
            // Handles the mouseover
            function() {
                $(this).prevAll().andSelf().addClass('ratings_hover');
                // $(this).nextAll().removeClass('ratings_vote'); 
            },
            function() {
                $(this).prevAll().andSelf().removeClass('ratings_hover');
                // set_votes($(this).parent());
            }
        );

		$('.ratings_stars').click(function(){
			var Values =  $(this).find("input").val();
	    	if ($(this).hasClass('ratings_over')) {
	            $('.ratings_stars').removeClass('ratings_over');
	            $(this).prevAll().andSelf().addClass('ratings_over');
	        } else {
	        	$(this).prevAll().andSelf().addClass('ratings_over');
	        }
	    });
	});
</script>

<script type="text/javascript">
	$(document).ready(function(){

		$(".subcomment").click(function(e){
			e.preventDefault();
			if ($(this).next().hasClass('show')) {
				$(this).next().removeClass('show');
			}
			else {
				$(this).next().addClass('show');
			}
		});

		$(".addcomment").click(function(e){
			var login = '{{Auth::check()}}';
			if (login == '') {
				alert('Bạn cần đăng nhập để bình luận');
				return false;
			}
			else {
				return true;
			}
		});

		$('.ratings_stars').click(function(){
				var rate = $(this).find('input').val();
				var blogID = "{{$getBlog['id']}}";
				// var _token = $('input[name="_token"]').val();
				$.ajaxSetup({
					headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					method: 'post',
					url: '/blog/detail/' + blogID + '/rate',
					data: {'rate' : rate},
					success: function($data){
						if ($data == 0) {
							alert('Bạn đã đánh giá bài viết này rồi');
							return false;
						}
						else {
							alert('Cảm ơn bạn đã đánh giá bài viết này');
							$('#average-rate').text($data + ' votes');
							return false;
						}
					},
					error: function($error){
						alert($error);
						return false;
					},
				});
			})
	});
</script>
	<div class="col-sm-9">
		<div class="blog-post-area">
			<h2 class="title text-center">Latest From our Blog</h2>
			<div class="single-blog-post">
				<h3>{{$getBlog['title']}}</h3>
				<div class="post-meta">
					<ul>
						<li><i class="fa fa-user"></i> {{$getBlog['user_name']}}</li>
						<li><i class="fa fa-clock-o"></i> {{$getBlog['updated_at']->toTimeString()}}</li>
						<li><i class="fa fa-calendar"></i> {{$getBlog['updated_at']->toDateString()}}</li>
					</ul>
				</div>
				<a href="">
					<img style="height: 390px" src="{{ asset('upload/user/avatar/'.$getBlog['image']) }}" alt="">
				</a>
				<p>{{$getBlog['description']}}</p>
			</div>
		</div><!--/blog-post-area-->

		<div class="rating-area">
			<ul class="ratings">
				<li class="rate-this">Rate this item:</li>
				<li>
					<div class="rate">
		                <div class="vote">
		                    <div class="star_1 ratings_stars"><input value="1" type="hidden"></div>
		                    <div class="star_2 ratings_stars"><input value="2" type="hidden"></div>
		                    <div class="star_3 ratings_stars"><input value="3" type="hidden"></div>
		                    <div class="star_4 ratings_stars"><input value="4" type="hidden"></div>
		                    <div class="star_5 ratings_stars"><input value="5" type="hidden"></div>
		                </div> 
		            </div>
				</li>
				<li id="average-rate" class="color">({{$average}} votes)</li>
			</ul>
			<ul class="tag">
				<li>TAG:</li>
				<li><a class="color" href="">Pink <span>/</span></a></li>
				<li><a class="color" href="">T-Shirt <span>/</span></a></li>
				<li><a class="color" href="">Girls</a></li>
			</ul>
		</div><!--/rating-area-->

		<div class="socials-share">
			<a href=""><img src="{{ asset('frontend/images/blog/socials.png')}}" alt=""></a>
		</div><!--/socials-share-->

		<div class="media commnets">
			<a class="pull-left" href="#">
				<img class="media-object" src="{{ asset('frontend/images/blog/man-one.jpg')}}" alt="">
			</a>
			<div class="media-body">
				<h4 class="media-heading">Annie Davis</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				<div class="blog-socials">
					<ul>
						<li><a href=""><i class="fa fa-facebook"></i></a></li>
						<li><a href=""><i class="fa fa-twitter"></i></a></li>
						<li><a href=""><i class="fa fa-dribbble"></i></a></li>
						<li><a href=""><i class="fa fa-google-plus"></i></a></li>
					</ul>
					<a class="btn btn-primary" href="">Other Posts</a>
				</div>
			</div>
		</div><!--Comments-->
		<div class="response-area">
			<h2>RESPONSES</h2>
			<ul class="media-list">

<!-- Comment BIG -->

				@foreach($getComment as $comment)
					<li class="media">
						<a class="pull-left" href="#">
							<img style="width: 121px; height: 86px;" class="media-object" src="{{ asset('upload/user/avatar/'.$comment['avatar']) }}" alt="">
						</a>
						<div class="media-body">
							<ul class="sinlge-post-meta">
								<li><i class="fa fa-user"></i>{{$comment['user_name']}}</li>
								<li><i class="fa fa-clock-o"></i>{{$comment['updated_at']->toTimeString()}}</li>
								<li><i class="fa fa-calendar"></i>{{$comment['updated_at']->toDateString()}}</li>
							</ul>
							<p>{{$comment['comment']}}</p>

<!-- Form Comment Small -->

							<a id="subcomment" class="btn btn-primary subcomment"><i class="fa fa-reply"></i>Replay</a>
							<div id="show" style="display: none;" class="text-area">
								<form method="post" action="">
									@csrf
									<div class="blank-arrow">
										<label>Comment</label>
									</div>
									<span>*</span>
									<input type="hidden" name="comment_id" value="{{$comment['id']}}">
									<textarea type="text" name="comment" rows="3"></textarea>
									<button type="submit" name="addcomment" class="btn btn-primary">post comment</button>
								</form>
							</div>
						</div>
					</li>

<!-- Comment small -->

					@foreach($getSubComment as $subcomment)
						@if($subcomment['comment_id'] == $comment['id'])
							<li class="media second-media">
								<a class="pull-left" href="#">
									<img style="width: 121px; height: 86px;" class="media-object" src="{{ asset('upload/user/avatar/'.$subcomment['avatar']) }}" alt="">
								</a>
								<div class="media-body">
									<ul class="sinlge-post-meta">
										<li><i class="fa fa-user"></i>{{$subcomment['user_name']}}</li>
										<li><i class="fa fa-clock-o"></i>{{$subcomment['updated_at']->toTimeString()}}</li>
										<li><i class="fa fa-calendar"></i>{{$subcomment['updated_at']->toDateString()}}</li>
									</ul>
									<p>{{$subcomment['comment']}}</p>
								</div>
							</li>
						@endif
					@endforeach
				@endforeach
			</ul>					
		</div><!--/Response-area-->
		<div class="replay-box">
			<div class="row">
				<div class="col-sm-12">
				<!-- <div class="col-sm-4"> -->
					<h2>Leave a replay</h2>
					<!-- <form>
						<div class="blank-arrow">
							<label>Your Name</label>
						</div>
						<span>*</span>
						<input type="text" placeholder="write your name...">
						<div class="blank-arrow">
							<label>Email Address</label>
						</div>
						<span>*</span>
						<input type="email" placeholder="your email address...">
						<div class="blank-arrow">
							<label>Web Site</label>
						</div>
						<input type="email" placeholder="current city...">
					</form> -->
				<!-- </div>
				<div class="col-sm-8"> -->
					<div class="text-area">

<!-- Form Comment Big -->

						<form style="margin-top: -45px;" id="addcomment" method="post" action="">
							@csrf
							<div class="blank-arrow">
								<label>Comment</label>
							</div>
							<span>*</span>
							<input type="hidden" name="comment_id" value="0">
							<textarea type="text" name="comment" rows="11"></textarea>

							@if($errors->any())
                                <div class="alert alert-danger alert-dismissible">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

							<button type="submit" name="addcomment" class="btn btn-primary addcomment">post comment</button>
						</form>
					</div>
				</div>
			</div>
		</div><!--/Repaly Box-->
	</div>	
@endsection