@extends('frontend.layouts.apps')
@section('content')
<script type="text/javascript">
        $(document).ready(function(){
            $('li.choose').click(function(e){
                e.preventDefault();
                var getName = $(this).find('img').attr('name');
                var getProductUserId = "{{$getProduct['user_id']}}";
                var getSrc = "{{ asset('upload/product/') }}"+ "/" + getProductUserId +"/"+ getName;
                var getHref = "{{ asset('upload/product/') }}"+ "/" + getProductUserId +"/3"+ getName;
                $('.frame').attr('src', getSrc);
                $('.zoom').attr('href', getHref);
            });
        });
    </script>
	<div class="col-sm-9 padding-right">
		<div class="product-details"><!--product-details-->
			<div class="col-sm-5">
				<div class="view-product">
					<img class="frame" src="{{ asset('upload/product/'.$getProduct['user_id'].'/'.$getImage[0]) }}" alt="" />
					<a class="zoom" href="{{ asset('upload/product/'.$getProduct['user_id'].'/3'.$getImage[0]) }}" rel="prettyPhoto"><h3>ZOOM</h3></a>
					
				</div>
				<div id="similar-product" class="carousel slide" data-ride="carousel">
					
					  <!-- Wrapper for slides -->
					    <div class="carousel-inner">
							<div class="item active">
								<ul style="padding-left: 34px">
									@foreach($getImage as $image)
										<li style="display: inline-block;" class="choose"><img name="{{$image}}" 
											style="width: 85px; height: 85px;display: inline-block; margin-left: 0;" src="{{ asset('upload/product/'.$getProduct['user_id'].'/2'.$image) }}" alt=""></li>
									@endforeach
								</ul>
							</div>
							<div class="item">
								<ul style="padding-left: 34px">
									@foreach($getImage as $image)
										<li style="display: inline-block;" class="choose"><img name="{{$image}}"  style="width: 85px; height: 85px;display: inline-block; margin-left: 0;" src="{{ asset('upload/product/'.$getProduct['user_id'].'/2'.$image) }}" alt=""></li>
									@endforeach
								</ul>
							</div>
						</div>

					  <!-- Controls -->
					  <a class="left item-control" href="#similar-product" data-slide="prev">
						<i class="fa fa-angle-left"></i>
					  </a>
					  <a class="right item-control" href="#similar-product" data-slide="next">
						<i class="fa fa-angle-right"></i>
					  </a>
				</div>
			</div>
			<div class="col-sm-7">
				<div class="product-information"><!--/product-information-->
					<h2>{{$getProduct['name']}}</h2>
					<!-- <p>Web ID: 1089772</p> -->
					<img src="{{ asset('frontend/images/product-details/rating.png')}}" alt="" />
					<span>
						<span>{{$getProduct['price']}}đ</span>
						<label>Quantity:</label>
						<input type="text" value="3" />
						<button type="button" class="btn btn-fefault cart">
							<i class="fa fa-shopping-cart"></i>
							Add to cart
						</button>
					</span>
					<p><b>Availability:</b> In Stock</p>
					<p><b>Condition:</b> New</p>
					<p><b>Brand:</b> E-SHOPPER</p>
					<a href=""><img src="{{ asset('frontend/images/product-details/share.png')}}" class="share img-responsive"  alt="" /></a>
				</div><!--/product-information-->
			</div>
		</div>
		<div class="category-tab shop-details-tab"><!--category-tab-->
			<div class="col-sm-12">
				<ul class="nav nav-tabs">
					<li><a href="#details" data-toggle="tab">Details</a></li>
					<li><a href="#companyprofile" data-toggle="tab">Company Profile</a></li>
					<li><a href="#tag" data-toggle="tab">Tag</a></li>
					<li class="active"><a href="#reviews" data-toggle="tab">Reviews (5)</a></li>
				</ul>
			</div>
			<div class="tab-content">
				<div class="tab-pane fade" id="details" >
					<div class="col-sm-3">
						<div class="product-image-wrapper">
							<div class="single-products">
								<div class="productinfo text-center">
									<img src="images/home/gallery1.jpg" alt="" />
									<h2>$56</h2>
									<p>Easy Polo Black Edition</p>
									<button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="companyprofile" >
					<div class="col-sm-3">
						<div class="product-image-wrapper">
							<div class="single-products">
								<div class="productinfo text-center">
									<img src="images/home/gallery1.jpg" alt="" />
									<h2>$56</h2>
									<p>Easy Polo Black Edition</p>
									<button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="tag" >
					<div class="col-sm-3">
						<div class="product-image-wrapper">
							<div class="single-products">
								<div class="productinfo text-center">
									<img src="images/home/gallery1.jpg" alt="" />
									<h2>$56</h2>
									<p>Easy Polo Black Edition</p>
									<button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade active in" id="reviews" >
					<div class="col-sm-12">
						<ul>
							<li><a href=""><i class="fa fa-user"></i>EUGEN</a></li>
							<li><a href=""><i class="fa fa-clock-o"></i>12:41 PM</a></li>
							<li><a href=""><i class="fa fa-calendar-o"></i>31 DEC 2014</a></li>
						</ul>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
						<p><b>Write Your Review</b></p>
						
						<form action="#">
							<span>
								<input type="text" placeholder="Your Name"/>
								<input type="email" placeholder="Email Address"/>
							</span>
							<textarea name="" ></textarea>
							<b>Rating: </b> <img src="images/product-details/rating.png" alt="" />
							<button type="button" class="btn btn-default pull-right">
								Submit
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="recommended_items"><!--recommended_items-->
			<h2 class="title text-center">recommended items</h2>
			
			<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<div class="item active">	
						@foreach($getProducts as $products)
							<div class="col-sm-4">
								<div class="product-image-wrapper">
									<div class="single-products">
										<div class="productinfo text-center">
											<img src="{{ asset('upload/product/'.$products['user_id'].'/'.$getImages[$products['id']][0]) }}" alt="" />
											<h2>{{$products['price']}}đ</h2>
											<p>{{$products['name']}}</p>
											<button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					</div>
					<div class="item">	
						@foreach($getProducts as $products)
							<div class="col-sm-4">
								<div class="product-image-wrapper">
									<div class="single-products">
										<div class="productinfo text-center">
											<img src="{{ asset('upload/product/'.$products['user_id'].'/'.$getImages[$products['id']][0]) }}" alt="" />
											<h2>{{$products['price']}}đ</h2>
											<p>{{$products['name']}}</p>
											<button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					</div>
				</div>
				 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
					<i class="fa fa-angle-left"></i>
				  </a>
				  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
					<i class="fa fa-angle-right"></i>
				  </a>			
			</div>
		</div><!--/recommended_items-->
	</div>
@endsection

