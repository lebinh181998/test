<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Home | E-Shopper</title>
    <link href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/prettyPhoto.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/price-range.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/animate.css') }}" rel="stylesheet">
	<link href="{{ asset('frontend/css/main.css') }}" rel="stylesheet">
	<link href="{{ asset('frontend/css/responsive.css') }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('frontend/images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('frontend/images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('frontend/images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('frontend/images/ico/apple-touch-icon-57-precomposed.png') }}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link type="text/css" rel="stylesheet" href="{{ asset('frontend/css/rate.css')}}">
</head><!--/head-->

<body>
    @include('frontend.layouts.header')

    <script type="text/javascript">
        $(document).ready(function(){

            $('#status').click(function(e){
                e.preventDefault();
                var choose = $(this).val();
                if (choose == 1) {
                    $('#sale').show();
                }
                else {
                    $('#sale').hide();
                }
            });
        });
    </script>

    <section style="margin-bottom: 100px;">
        <div class="container">
            <div class="row"> 
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a id="profile" href="{{url('account/profile')}}">Profile</a></h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a id="myproduct" href="{{url('account/myproduct')}}">My product</a></h4>
                                </div>
                            </div>
                        </div><!--/category-productsr-->
                    </div>
                </div>

                <div class="col-sm-9">

                    @if($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
					<!-- ----------------------------ADD NEW PRODUCT-------------------------------- -->
                    <div class="signup-form"><!--features_items-->
                        <h2 style="color: #FE980F;font-family: 'Roboto', sans-serif; font-size: 18px;font-weight: 700;" class="title text-center">ADD NEW</h2>
                        <form method="post" action="" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-12">Name</label>
                                <div class="col-md-12">
                                    <input type="text"  class="form-control form-control-line" name="name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Select Category</label>
                                <div class="col-sm-12">
                                    <select name="category" class="form-control form-control-line">
                                        @foreach($getCategory as $category)
                                            <option value="{{$category['id']}}">{{ $category['name'] }}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Select Brand</label>
                                <div class="col-sm-12">
                                    <select name="brand" class="form-control form-control-line">
                                        @foreach($getBrand as $brand)
                                            <option value="{{$brand['id']}}">{{ $brand['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Select Sale</label>
                                <div class="col-sm-12">
                                    <select id="status" name="status" class="form-control form-control-line">
                                        @foreach($getSale as $sale)
                                            <option value="{{$sale['status']}}">{{ $sale['name'] }}</option>
                                        @endforeach
                                    </select>
                                    <input id="sale" style="display: none; width: 10%;" type="text" name="sale">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Tag</label>
                                <div class="col-md-12">
                                    <input type="text" name="tag" class="form-control form-control-line">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Price</label>
                                <div class="col-md-12">
                                    <input type="text" name="price" class="form-control form-control-line">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Image</label>
                                <div class="col-md-12">
                                    <input type="file" name="image[]" multiple>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Detail</label>
                                <div class="col-md-12">
                                    <textarea name="detail" rows="11"> </textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button style="float: right;" name="addproduct" class="btn btn-success">Add Product</button>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
            </div>
        </div>
    </section>
    @include('frontend.layouts.footer')

    <script src="{{ asset('frontend/js/jquery.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.scrollUp.min.js') }}"></script>
    <script src="{{ asset('frontend/js/price-range.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('frontend/js/main.js') }}"></script>
</body>
</html>