<!DOCTYPE html>
<html>
<head>
	<title>Register</title>

    <link href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/prettyPhoto.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/price-range.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/animate.css') }}" rel="stylesheet">
	<link href="{{ asset('frontend/css/main.css') }}" rel="stylesheet">
	<link href="{{ asset('frontend/css/responsive.css') }}" rel="stylesheet">
	<link rel="shortcut icon" href="images/ico/favicon.ico') }}">

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('frontend/images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('frontend/images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('frontend/images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('frontend/images/ico/apple-touch-icon-57-precomposed.png') }}">

    <script src="{{ asset('frontend/js/jquery.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.scrollUp.min.js') }}"></script>
    <script src="{{ asset('frontend/js/price-range.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('frontend/js/main.js') }}"></script>
</head>
<body>
	<header style="margin-bottom: 100px" id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> info@domain.com</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-md-4 clearfix">
						<div class="logo pull-left">
							<a href="{{url('/')}}"><img src="{{ asset('frontend/images/home/logo.png') }}" alt="" /></a>
						</div>
						<div class="btn-group pull-right clearfix">
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									USA
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="">Canada</a></li>
									<li><a href="">UK</a></li>
								</ul>
							</div>
							
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									DOLLAR
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="">Canadian Dollar</a></li>
									<li><a href="">Pound</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-8 clearfix">
						<div class="shop-menu clearfix pull-right">
							<ul class="nav navbar-nav">
								@if(Auth::check())
								<li><a href="{{url('account/profile')}}"><i class="fa fa-user"></i> Account</a></li>
								@endif
								<li><a href=""><i class="fa fa-star"></i> Wishlist</a></li>
								<li><a href="checkout.html"><i class="fa fa-crosshairs"></i> Checkout</a></li>
								<li><a href="{{url('cart')}}"><i class="fa fa-shopping-cart"></i> Cart</a></li>
								@if(!Auth::check())
								<li><a href="{{url('member/login')}}"><i class="fa fa-lock"></i> Login</a></li>
								<li><a href="{{url('member/register')}}"><i class="fa fa-lock"></i> Register</a></li>
								@else
								<li><a href="{{url('member/logout')}}"><i class="fa fa-lock"></i> Logout</a></li>
								@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->

	</header><!--/header-->

    <div style="margin-bottom: 100px" class="container">
    	<div class="row">
    		
    		@if($errors->any())
		        <div class="alert alert-danger alert-dismissible">
		            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		            <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
		            <ul>
		                @foreach($errors->all() as $error)
		                    <li>{{$error}}</li>
		                @endforeach
		            </ul>
		        </div>
		    @endif

    		<div  class="col-sm-offset-2 col-md-8 ">
    			<form method="post" action="" enctype="multipart/form-data" class="form-horizontal form-material">
			    @csrf
			    <div class="form-group">
			        <label class="col-md-12">Full Name</label>
			        <div class="col-md-12">
			            <input type="text" placeholder="" class="form-control form-control-line" name="name" >
			        </div>
			    </div>
			    <div class="form-group">
			        <label for="example-email" class="col-md-12">Email</label>
			        <div class="col-md-12">
			            <input type="email" placeholder="" class="form-control form-control-line"  name="email" id="email">
			        </div>
			    </div>
			    <div class="form-group">
			        <label class="col-md-12">Password</label>
			        <div class="col-md-12">
			            <input type="password" name="password" class="form-control form-control-line">
			        </div>
			    </div>
			     <div class="form-group">
			        <label class="col-md-12">Password Confirm</label>
			        <div class="col-md-12">
			            <input type="password" name="password-c" class="form-control form-control-line">
			        </div>
			    </div>
			    <div class="form-group">
			        <label class="col-md-12">Phone</label>
			        <div class="col-md-12">
			            <input type="text" name="phone" placeholder="" class="form-control form-control-line">
			        </div>
			    </div>
			    <div class="form-group">
			        <label class="col-md-12">Avatar</label>
			        <div class="col-md-12">
			            <input type="file" name="avatar" >
			        </div>
			    </div>
			    <div class="form-group">
			        <label class="col-sm-12">Select Country</label>
			        <div class="col-sm-12">
			            <select name="country" class="form-control form-control-line">
			                
			            </select>
			        </div>
			    </div>
			    <div class="form-group">
			        <div class="col-sm-12">
			            <button name="register" class="btn btn-success">Register</button>
			        </div>
			    </div>
			</form>
    		</div>
    	</div>
    </div>
	
</body>
</html>