<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	
    <link href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/prettyPhoto.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/price-range.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/animate.css') }}" rel="stylesheet">
	<link href="{{ asset('frontend/css/main.css') }}" rel="stylesheet">
	<link href="{{ asset('frontend/css/responsive.css') }}" rel="stylesheet">
	<link rel="shortcut icon" href="images/ico/favicon.ico') }}">

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('frontend/images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('frontend/images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('frontend/images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('frontend/images/ico/apple-touch-icon-57-precomposed.png') }}">

    <script src="{{ asset('frontend/js/jquery.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.scrollUp.min.js') }}"></script>
    <script src="{{ asset('frontend/js/price-range.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('frontend/js/main.js') }}"></script>

    <style type="text/css">
    	.card {
		    position: relative;
		    display: flex;
		    flex-direction: column;
		    min-width: 0;
		    word-wrap: break-word;
		    background-color: #fff;
		    background-clip: border-box;
		    border: 1px solid rgba(0, 0, 0, 0.125);
		    border-radius: 0.25rem;
		}
		.card-header:first-child {
		    border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
		}
		.card-header {
		    padding: 0.75rem 1.25rem;
		    margin-bottom: 0;
		    background-color: rgba(0, 0, 0, 0.03);
		    border-bottom: 1px solid rgba(0, 0, 0, 0.125);
		}
		.card-body {
		    flex: 1 1 auto;
		    min-height: 1px;
		    padding: 1.25rem;
		}
		.col-form-label {
		    padding-top: calc(0.375rem + 1px);
		    padding-bottom: calc(0.375rem + 1px);
		    margin-bottom: 0;
		    font-size: inherit;
		    line-height: 1.6;
		}
		.text-md-right {
		    text-align: right !important;
		}
    </style>
</head>
<body>
	<header><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> info@domain.com</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-md-4 clearfix">
						<div class="logo pull-left">
							<a href="{{url('/')}}"><img src="{{ asset('frontend/images/home/logo.png') }}" alt="" /></a>
						</div>
						<div class="btn-group pull-right clearfix">
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									USA
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="">Canada</a></li>
									<li><a href="">UK</a></li>
								</ul>
							</div>
							
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									DOLLAR
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="">Canadian Dollar</a></li>
									<li><a href="">Pound</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-8 clearfix">
						<div class="shop-menu clearfix pull-right">
							<ul class="nav navbar-nav">
								@if(Auth::check())
								<li><a href="{{url('account/profile')}}"><i class="fa fa-user"></i> Account</a></li>
								@endif
								<li><a href=""><i class="fa fa-star"></i> Wishlist</a></li>
								<li><a href="checkout.html"><i class="fa fa-crosshairs"></i> Checkout</a></li>
								<li><a href="{{url('cart')}}"><i class="fa fa-shopping-cart"></i> Cart</a></li>
								@if(!Auth::check())
								<li><a href="{{url('member/login')}}"><i class="fa fa-lock"></i> Login</a></li>
								<li><a href="{{url('member/register')}}"><i class="fa fa-lock"></i> Register</a></li>
								@else
								<li><a href="{{url('member/logout')}}"><i class="fa fa-lock"></i> Logout</a></li>
								@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->

	</header><!--/header-->

    <div class="container">
    	<div class="row">
    		<div class="col-sm-offset-2 col-md-8">

    			@if($errors->any())
			        <div class="alert alert-danger alert-dismissible">
			            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			            <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
			            <ul>
			                @foreach($errors->all() as $error)
			                    <li>{{$error}}</li>
			                @endforeach
			            </ul>
			        </div>
			    @endif

    			<div class="card">
    				<div class="card-header">Register</div>
    				<div class="card-body">
    					<form  method="post" action="" enctype="multipart/form-data" class="form-horizontal form-material">
						    @csrf
						    <div class="form-group row">
						        <label class="col-md-4 col-form-label text-md-right" for="example-email">Email</label>
						        <div class="col-md-6">
						            <input type="email" placeholder="" class="form-control form-control-line"  name="email">
						        </div>
						    </div>
						    <div class="form-group row">
						        <label class="col-md-4 col-form-label text-md-right">Password</label>
						        <div class="col-md-6">
						            <input type="password" name="password" class="form-control form-control-line">
						        </div>
						    </div>
						    <div class="form-group row">
						        <div style="text-align: center;" class="col-sm-12">
						            <button name="login" class="btn btn-success">Login</button>
						        </div>
						    </div>
						</form>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</body>
</html>