@extends('frontend.layouts.apps')
@section('content')
	<div class="col-sm-9 padding-right">
		<div class="features_items"><!--features_items-->
			<h2 class="title text-center">Features Items</h2>
			@foreach($getProduct as $product)
			<div class="col-sm-4">
				<div class="product-image-wrapper">
					<div class="single-products">
						<div class="productinfo text-center">
							<a href="{{url('product/detail/'.$product['id'])}}"><img class="" src="{{asset('upload/product/'.$product['user_id'].'/'.$getImage[$product['id']][0])}}" alt="" /></a>

							<h2>{{$product['price']}}đ</h2>
							<a href="{{url('product/detail/'.$product['id'])}}"><p>{{$product['name']}}</p></a>
							<a id="{{$product['id']}}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
		<div style=" text-align: center;">{{$getProduct->links()}}</div>
	</div>
@endsection