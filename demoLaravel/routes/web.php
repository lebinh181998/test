<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
	    'prefix' => 'admin',
	    'namespace' => 'Auth',
	    'middleware' => ['admin'],
	], function () {
	    Route::get('/', 'LoginController@showLoginForm');
	    Route::get('/login', 'LoginController@showLoginForm');
	    Route::post('/login', 'LoginController@login');
	    Route::get('/logout', 'LoginController@logout');
});

Route::group([
		'namespace' =>'Frontend',
	],
	function(){
//Blog
		Route::get('/blog','BlogController@list')->name('frontend.blog');
		Route::get('/blog/detail/{id}','BlogController@detail');
//Index
		Route::get('/','ProductController@index');
		Route::post('/ajax-addtocart','ProductController@addtocart');
		Route::get('/cart','ProductController@cart');
		Route::post('/ajax-cart/flus','ProductController@flus');
		Route::post('/ajax-cart/minus','ProductController@minus');
		Route::post('/ajax-cart/delete','ProductController@deleteSSProduct');
//Product
		Route::get('/product/detail/{id}','ProductController@detail');
//Search
		Route::get('/search','ProductController@search');
		Route::post('/search','ProductController@searchResults');
		Route::post('/search/price','ProductController@priceSearch');
//Checkout
		Route::get('/checkout','ProductController@showCheckout');
		Route::post('/checkout','ProductController@checkout_success');
});

Route::group([
		// 'prefix' => 'frontend',
		'namespace' =>'Frontend',
		'middleware' => ['member'],
	],
	function(){
//Account
		Route::get('/account/profile','MemberController@edit');
		Route::post('/account/profile','MemberController@update');
		Route::get('/account/myproduct','MemberController@showMyProduct');
//Product
		Route::get('/product/add','ProductController@productForm');
		Route::post('/product/add','ProductController@addproduct');

		Route::get('/product/edit/{id}','ProductController@edit');
		Route::post('/product/edit/{id}','ProductController@update');

		Route::post('/product/delete','ProductController@delete');
//Blog
		Route::post('/blog/detail/{id}','BlogController@addcomment');
		Route::post('/blog/detail/{id}/rate','BlogController@addrate');
//Logout
		Route::get('/member/logout', 'MemberController@mblogout');
});

Route::group([
		// 'prefix' => 'frontend',
		'namespace' =>'Frontend',
		'middleware' => ['notlogin'],
	],
	function(){
//Account
		Route::get('/member/register','MemberController@create')->name('frontend.member.mbregister');
		Route::post('/member/register','MemberController@create_success');

		Route::get('/member/login','MemberController@index');
		Route::post('/member/login','MemberController@mblogin');
});
//gio e login vao dc roi thi e moi vao dc trang home
//con neu e lgout ra thi e vao k dc. no seda ve trang login
//va trong laravel routet anc.com/login va register da dc dung, neen sau nay e co lam login thi dat ten khac,
//vdu: abc.com/member/login...
Auth::routes();



Route::group([
		'prefix' => 'admin',
		'namespace' =>'Admin',
		'middleware' => ['admin'],
	],
	function(){
//Home
		Route::get('/home', 'HomeController@index')->name('home');
//Dashboard
		Route::get('/dashboard','DashboardController@index')->name('admin.dashboard');

//Profile
		Route::get('/user/profile','UserController@edit')->name('admin.user.edit');
		Route::post('/user/profile','UserController@update')->name('admin.user.update');
//Blog
		Route::get('/blog','BlogController@list')->name('admin.blog');

		Route::get('/blog/add','BlogController@create')->name('admin.blog.add');
		Route::post('/blog/add','BlogController@create_success');

		Route::get('/blog/edit/{id}','BlogController@edit');
		Route::post('/blog/edit/{id}','BlogController@update');

		Route::get('/blog/delete/{id}','BlogController@delete');
//Country
		Route::get('/country', 'CountryController@country');
		Route::post('/country', 'CountryController@addCountry');

		Route::get('/country/delete/{id}', 'CountryController@deleteCountry');
//Category
		Route::get('/category', 'CategoryController@index');
		Route::post('/category', 'CategoryController@add');
//Brand
		Route::get('/brand', 'BrandController@index');
		Route::post('/brand', 'BrandController@add');
//Sale
		Route::get('/sale', 'SaleController@index');
		Route::post('/sale', 'SaleController@add');
});




