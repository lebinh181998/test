<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([
	'namespace' => 'API',
	// 'middleware' => 'api',
	],
	function(){
//Home
		Route::get('/index', 'ProductController@index');
//Product
		Route::get('/product/add', 'ProductController@addForm');
		Route::post('/product/add', 'ProductController@add');
		Route::get('/product/edit/{id}', 'ProductController@editForm');
		Route::post('/product/edit/{id}', 'ProductController@edit');
		Route::post('/product/delete/{id}', 'ProductController@delete');
		Route::post('/product/cart', 'ProductController@viewCart');
//Blog
		Route::get('/blog', 'BlogController@list');
		Route::get('/blog/detail/{id}', 'BlogController@detail');

		Route::post('/blog/comment/{id}', 'BlogController@addComment');

		Route::get('/blog/rate/{id}', 'BlogController@rate');
		Route::post('/blog/rate/{id}', 'BlogController@addRate');
//Account
		Route::post('/profile/update', 'MemberController@updateProfile');
		Route::get('/myproduct/{id}', 'MemberController@listMyProduct');
//Login
		Route::post('/member/login', 'MemberController@login');
//Register
		Route::post('/member/register', 'MemberController@register');
});