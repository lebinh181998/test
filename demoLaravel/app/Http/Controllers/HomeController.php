<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   // ham __construct e doc ben OOP roi, nghia la no se khoi tao va chay dau tien.
    // public function __construct()
    // {
    //     //cai nay la no check xem user da login vao chua neu chua thi no da ve trang login.
    //     //khi chay lenh hoi nay xong, thi no cung tao san cho e 2 man hinh login va register luon, va nhung cai nay
    //     //e han che chinh sua neu k hieu. doan ni e hieu k?
    //     //da hieu,
    //     $this->middleware('auth');
    // }
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    //           profile
    public function profile()
    {
        return view('admin/profile');
    }

    //           GET PROFILE
    public function getgprofile()
    {
        $dataprof = profile::all();
        return view('admin/profile', compact('dataprof'));
    }

    public function test($id)
    {
        $dataprof = User::all();
        return view('admin/test', compact('dataprof'));
    }
    // em đang lấy dữ liệu từ table users đưa lên profile hôm qua anh nói

}
