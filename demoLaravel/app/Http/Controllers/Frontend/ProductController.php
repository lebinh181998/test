<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\frontend\InsertProductRequest;
use App\Http\Requests\frontend\UpdateProductRequest;
use App\Models\Product;
use App\Models\History;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Sale;
use Auth;
use Mail;
use DB;
use Intervention\Image\Facades\Image as Image;

class ProductController extends Controller
{
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // echo "<pre>";
        // var_dump(session()->get('cart'));
        // echo "</pre>";
        $getProduct = Product::paginate(6);
        foreach ($getProduct as  $product) {
            $getImage[$product['id']] = json_decode($product['image']);
        }
        return view('frontend.index', compact('getProduct', 'getImage'));
    }

    public function search()
    {
        // Mail::send('sendMail.mail', array(
        //     'slug' => 'tesst',
        //     'content' => "aaaaa",
        // ), function($message){
        //     $message->to('thienbaoit@gmail.com', 'dang thien bao')->subject('thienbaoit test mail!');
        // });
        //price, name, trang thai, category, brands
        $getCategory = Category::All()->toArray();
        $getBrand = Brand::All()->toArray();
        $getSale = Sale::All()->toArray();
        return view('frontend.search', compact('getCategory', 'getBrand', 'getSale'));
        //Enter url mất kq search
        //chưa search riêng từng phần tử dc
        //return k đúng chỗ redirect()->back();
        //checkout lâu
    }

    public function priceSearch(Request $request)
    {
        $getCategory = Category::All()->toArray();
        $getBrand = Brand::All()->toArray();
        $getSale = Sale::All()->toArray();

        $price = $request->price;
        $getPrice = explode(' : ', $price);
        $getDBProduct = Product::whereBetween('price',[ $getPrice[0], $getPrice[1] ])->get();

        foreach ($getDBProduct as  $product) {
            $getImage[$product['id']] = json_decode($product['image']);
        }

        return $getDBProduct;
    }

    public function searchResults(Request $request)
    {

        $getCategory = Category::All()->toArray();
        $getBrand = Brand::All()->toArray();
        $getSale = Sale::All()->toArray();

        $data = $request->all();
        $string = $request->string;
        
        $getDBProduct = DB::table('product');
        if(!empty($data['name'])){
            $getDBProduct->where('name', 'like', '%' . $data['name'] . '%');
        }
        if(!empty($data['price'])){
            $getDBProduct->where('price', '<=', $data['price']);
        }
        if(!empty($data['category'])){
            $getDBProduct->where('category_id', $data['category']);
        }
        if(!empty($data['brand'])){
            $getDBProduct->where('brand_id', $data['brand']);
        }
        if(!empty($data['status'])){
            $getDBProduct->where('status', $data['status']);
        }

        $getProduct = $getDBProduct->get()->toArray();
        //dd($getProduct);
        foreach ($getProduct as  $product) {
            $getImage[$product->id] = json_decode($product->image, true);
        }

        return view('frontend.search', compact('getProduct', 'getImage', 'getCategory', 'getBrand', 'getSale'));
    }

    public function cart()
    {
        $sum = 0;
        if(session()->has('cart')) {
            $getSSProduct = session()->get('cart');
            if (!empty($getSSProduct)) {
                foreach ($getSSProduct as $key => $value) {
                    $arrProductID[] = $value['id'];
                    $arrProductQTY[$value['id']] = $value['qty'];
                }
                $getProduct = Product::whereIn('id', $arrProductID)->get();

                foreach ($getProduct as  $product) {
                    $getImage[$product['id']] = json_decode($product['image']);
                    $sum += $arrProductQTY[$product['id']] * $product['price'];
                }
            }
        }
        return view('frontend.cart', compact('getProduct', 'getImage', 'arrProductQTY', 'sum'));
    }

    public function addtocart(Request $request)
    {
        $productID = $request->id;
        $product = array('id' => $productID, 'qty' => 1);
        $flag = true;
        if(session()->has('cart')) {
            $getSSProduct = session()->get('cart');
            foreach ($getSSProduct as $key => $value) {
                if ($productID == $value['id']) {
                    $flag = false;
                    $getSSProduct[$key]['qty'] += 1;
                    session()->put('cart', $getSSProduct);
                    break;
                }
            }
        }
        if ($flag) {
            session()->push('cart', $product);
        }      
    }

    public function flus(Request $request)
    {
        $productID = $request->id;
        $getSSProduct = session()->get('cart');
        foreach ($getSSProduct as $key => $value) {
            if ($productID == $value['id']) {
                $getSSProduct[$key]['qty'] += 1;
                session()->put('cart', $getSSProduct);
                return $getSSProduct[$key]['qty'];
                break;
            }
        }
    }

    public function minus(Request $request)
    {
        $productID = $request->id;
        $getSSProduct = session()->get('cart');
        foreach ($getSSProduct as $key => $value) {
            if ($productID == $value['id']) {
                if ($getSSProduct[$key]['qty'] >=1) {
                    $getSSProduct[$key]['qty'] -= 1;
                    session()->put('cart', $getSSProduct);
                    return $getSSProduct[$key]['qty'];
                    break;
                }
            }
        }
    }

    public function deleteSSProduct(Request $request)
    {
        $productID = $request->id;
        $getSSProduct = session()->get('cart');
        foreach ($getSSProduct as $key => $value) {
            if ($productID == $value['id']) {
                unset($getSSProduct[$key]);
                session()->put('cart', $getSSProduct);
                break;
            }
        }
    }

    public function showCheckout()
    {
        $sum = 0;
        if(session()->has('cart')) {
            $getSSProduct = session()->get('cart');
            if (!empty($getSSProduct)) {
                foreach ($getSSProduct as $key => $value) {
                    $arrProductID[] = $value['id'];
                    $arrProductQTY[$value['id']] = $value['qty'];
                }
                $getProduct = Product::whereIn('id', $arrProductID)->get();

                foreach ($getProduct as  $product) {
                    $getImage[$product['id']] = json_decode($product['image']);
                    $sum += $arrProductQTY[$product['id']] * $product['price'];
                }
            }
        }
        return view('frontend.checkout', compact('getProduct', 'getImage', 'arrProductQTY', 'sum')); 
    }

    public function checkout_success(Request $request)
    {

            if (session()->has('cart')) {
                $getSSProduct = session()->get('cart');
                if (!empty($getSSProduct)) {


                    // foreach ($getSSProduct as $key => $value) {
                    //     $arrProductID[] = $value['id'];
                    // }

                    // $getProduct = Product::whereIn('id', $arrProductID)->get();

                    // foreach ($getProduct as  $product) {
                    //     $getImage[$product['id']] = json_decode($product['image']);
                    // }

                    Mail::send('sendMail.mail', array(
                        'getTable' => $request->html,
                        // 'getProduct' => $getProduct,
                        // 'getImage' => $getImage,
                        // 'content' => $data['content']
                    ), function($message){
                        $message->to('binbon1998bb@gmail.com', 'lebinh')->subject('send mail!');
                    });

                    $history = new History();
                    $history->price_sum = $request->price_sum;
                    $history->user_id = Auth::id();
                    $history->save();
                    session()->forget('cart');
                    $success = 1;
                    return $success;
                }
                else {
                    $error = 'Không có sản phẩm để thanh toán1';
                    return $error;
                }
            }
            else {
                $error = 'Không có sản phẩm để thanh toán';
                return $error;
            }
        
 
        
        

        // ini_set( 'display_errors', 1 ); // 2 dong này de show lỗi trong file apache log
        // error_reporting( E_ALL ); // 2 dong này de show lỗi trong file apache  log

        // $from = "lebinh181998@gmail.com";
        // $to = "binbon1998bb@gmail.com";
        // $subject = "Checking send mail PHP";
        // $message = $request->html;
        // $headers = "From:" . $from;
        // if(mail($to,$subject,$message, $headers)){

        // } else {
        //     echo "error send mail";
        // }
    }

    public function detail($id)
    {
        $getProducts = Product::All()->take(3)->toArray();
        $getProduct = Product::find($id)->toArray();
        $getImage = json_decode($getProduct['image']);
        foreach ($getProducts as  $products) {
            $getImages[$products['id']] = json_decode($products['image']);
        }
        return view('frontend.product.detail', compact('getProduct', 'getImage', 'getProducts', 'getImages'));
    }

    public function productForm()
    {
        $getCategory = Category::All()->toArray();
        $getBrand = Brand::All()->toArray();
        $getSale = Sale::All()->toArray();
        return view('frontend.product.add', compact('getCategory', 'getBrand', 'getSale'));
    }

    public function uploadFile($file, $arrImage, $userId)
    {
        foreach ($file as $key => $image) {
            $name = $arrImage[$key];
            $name2 = '2'.$arrImage[$key];
            $name3 = '3'.$arrImage[$key];
            $path = public_path('upload/product/'.$userId.'/'.$name);
            $path2 = public_path('upload/product/'.$userId.'/'.$name2);
            $path3 = public_path('upload/product/'.$userId.'/'.$name3);
            Image::make($image->getRealPath())->resize(329,380)->save($path);
            Image::make($image->getRealPath())->resize(85,85)->save($path2);
            Image::make($image->getRealPath())->resize(350,400)->save($path3);
        }
    }

    public function addproduct(InsertProductRequest $request)
    {
        $data = $request->all();
        $product = new Product();
        $userId = Auth::id();
        
        $product->name = $data['name'];
        $product->category_id = $data['category'];
        $product->brand_id = $data['brand'];
        $product->status = $data['status'];

        if ($data['status'] == 1) {
            if (empty($data['sale'])) {
                return redirect('/product/add')->withErrors('Vui lòng nhập giảm giá');
            }
            $product->sale = $data['sale'];
        }
        else {
            $product->sale = 0;
        }
        
        $product->tag = $data['tag'];

        if ($request->hasfile('image')) {
             foreach ($request->file('image') as $image) {
                $name = strtotime(date('Y-m-d H:i:s'))."_".$image->getClientOriginalName();
                $arrImage[] =$name;
             }
             $product->image = json_encode($arrImage);
        }

        $product->price = $data['price'];
        $product->detail = $data['detail'];
        $product->user_id = $userId;

        if ($product->save()) {
            if ($request->hasfile('image')) {
                if (!file_exists('upload/product/'.$userId)) {
                     mkdir('upload/product/'.$userId);
                }
                $this->uploadFile($request->file('image'), $arrImage, $userId);
            }
            return redirect('/account/myproduct')->with('success', __('Add product success.'));
        }
    }

    public function delete(Request $request)
    {
        $productID = $request->id;
        $product = Product::find($productID);
        $product->delete();
        return 'Đã xóa sản phẩm';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userId = Auth::id();
        $getCategory = Category::All()->toArray();
        $getBrand = Brand::All()->toArray();
        $getSale = Sale::All()->toArray();
        $getProduct = Product::find($id)->toArray();
        $getImage = json_decode($getProduct['image']);
        return view('frontend.product.edit', compact('getProduct', 'userId', 'getImage', 'getCategory', 'getBrand', 'getSale'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InsertProductRequest $request, $id)
    {
        $data = $request->all();
        $userId = Auth::id();
        $product = Product::findOrFail($id);
        if ($product->count() > 0) {

            if ($data['status'] == 1) {
                if (empty($data['sale'])) {
                    return redirect('/product/edit/'.$id)->withErrors('Vui lòng nhập giảm giá');
                }
            }

            $getImage = json_decode($product['image']);
        
            if ($request->hasfile('image')) {

                foreach ($request->file('image') as $image) {
                    $name = strtotime(date('Y-m-d H:i:s'))."_".$image->getClientOriginalName();
                    $arrImage[] =$name;
                }
                $countFileImage = count($arrImage);
            }
            else {
                $countFileImage = 0;
                $arrImage = array();
            }

            if ($request->check) {
                foreach ($data['check'] as $check) {
                    unset($getImage[$check]);
                }
                $countDelImage =count($getImage);
            }
            else {
                $countDelImage = count($getImage);
            }

            $countImage = $countFileImage + $countDelImage;

            if ($countImage > 3) {
                $error = 'Ảnh cho sản phẩm không được quá 3';
                return redirect('/product/edit/'.$id)->withErrors($error);
            }
            else if ($countImage <= 0) {
                $error = 'Sản phẩm phải có ít nhất 1 ảnh';
                return redirect('/product/edit/'.$id)->withErrors($error);
            }
            else {
                $image = array_merge($getImage, $arrImage);
                $data['image'] = json_encode($image);
            }
//LỖI _TOKEN
            if ($product->update($data)) {
                if ($request->hasfile('image')) {
                    if (!file_exists('upload/product/'.$userId)) {
                         mkdir('upload/product/'.$userId);
                    }
                    $this->uploadFile($request->file('image'), $arrImage, $userId);
                }
                return redirect('/account/myproduct')->with('success', __('Update product success.'));
            }
        }
        else {
            return redirect('/account/myproduct');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
