<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\User;
use App\Models\Comment;
use App\Models\Rate;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    public function addrate(Request $request, $id)
    {
        if (Auth::check()) {
            $error = "";
            $data = $request->all();
            $userId = Auth::id();
            $rate = Rate::where('blog_id', $id)->get();
            $rate_user = Rate::where([ ['blog_id', $id], ['user_id', $userId] ])->get();

            $addrate = new Rate();
            $addrate->rate = $data['rate'];
            $addrate->user_id = $userId;
            $addrate->blog_id = $id;


            if ($rate->count() <= 0) {
                if($addrate->save()) {
                    $sumrate = 0;
                    foreach ($rate as $value) {
                        $sumrate += $value['rate'];
                    }
                    $average = round( ($sumrate + $data['rate'])/(count($rate) + 1) );
                    return $average;
                }
            }
            else {
                if($rate_user->count() > 0) {
                    $notice = 0;
                    return $notice;
                }
                else {
                    $error = "";
                    $addrate->save();
                    $sumrate = 0;
                    foreach ($rate as $value) {
                        $sumrate += $value['rate'];
                    }
                    $average = round( ($sumrate + $data['rate'])/(count($rate) + 1) );
                    return $average;
                }
            }
            
        }
        else {
            $error = 'Bạn cần đăng nhập để đánh giá';
            return $error;
        }
    }

    public function list()
    {
        $getBlog = Blog::paginate(3);
        return view('frontend.blog.list', compact('getBlog'));
    }

    public function detail($id)
    {
        $getBlog = Blog::find($id);
        $getComment = Comment::where([['blog_id', $id],['comment_id', '0']])->get();
        $getSubComment = Comment::where([['blog_id', $id],['comment_id', '<>', '0']])->get();
        $rate = Rate::where('blog_id', $id)->get();
        $sumrate = 0;
        if ($rate->count() > 0) {
            foreach ($rate as $value) {
                $sumrate += $value['rate'];
            }
            $average = round($sumrate/count($rate));
        }
        else {
            $average = 0;
        }
        
        

        return view('frontend.blog.detail', compact('getBlog', 'getComment', 'getSubComment', 'average'));
    }

    public function addcomment(Request $request, $id)
    {
        if (Auth::check()) {
            $this->validate($request,
                [
                    'comment' => 'required',
                ],

                [
                    'required' => 'Bạn chưa :attribute',
                ],

                [
                    'comment' => 'bình luận',
                ]
            );

            $data = $request->all();
            $userId = Auth::id();
            $user = User::findOrFail($userId);
            $comment = new Comment();

            $comment->avatar = $user['avatar'];
            $comment->user_name = $user['name'];
            $comment->comment = $data['comment'];
            $comment->comment_id = $data['comment_id'];
            $comment->user_id = $userId;
            $comment->blog_id = $id;

            if($comment->save()) {
                return redirect('/blog/detail/'.$id);
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
