<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\frontend\RegisterRequest;
use App\Http\Requests\frontend\LoginRequest;
use App\Models\User;
use App\Models\Country;
use App\Models\Product;
use App\Http\Requests\admin\UpdateProfileRequest;
use Auth;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.member.mblogin');
    }

    public function showMyProduct()
    {
        $stt = 1;
        $userId = Auth::id();
        $getProduct = Product::where('user_id', $userId)->get();
        foreach ($getProduct as $product) {
            $getImage[$product->id] = json_decode($product['image']);
        }
        return view('frontend.account.myproduct', compact('getProduct', 'userId', 'stt', 'getImage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.member.mbregister');
    }

    public function mblogin(LoginRequest $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            if (Auth::user()->level == 0) {
                return redirect('/');
            }
            else {
                Auth::logout();
                $error = 'Tài khoản hoặc mật khẩu sai';
                return redirect('/member/login')->withErrors($error);
            }
        }
        else {
            $error = 'Tài khoản hoặc mật khẩu sai';
            return redirect('/member/login')->withErrors($error);
        }
    }

    public function mblogout(Request $request)
    {
        Auth::logout();
        return redirect('/member/login');
    }

    public function create_success(RegisterRequest $request)
    {
        $data = $request->all();
        $user = new User();
        $file = $data['avatar'];

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->phone = $data['phone'];
        if (!empty($file)) {
             $user->avatar = $file->getClientOriginalName();
        }
        $user->level = 0;
        if ($user->save()) {
            if (!empty($file)) {
                $file->move('upload/user/avatar/', $file->getClientOriginalName());
            }
            
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                return redirect('/blog');
            }
            else {
                return redirect('/member/register');
            }
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $userId = Auth::id();
        $getUser = User::find($userId)->toArray();
        $getCountry = Country::All()->toArray();
        return view('frontend.account.profile', compact('getUser', 'getCountry'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfileRequest $request)
    {
        if (Auth::check()) {
            $userId = Auth::id();
            $user = User::findOrFail($userId);
            $data = $request->all();
            $file = $request->avatar;

            if (!empty($file)) {
                $data['avatar'] = $file->getClientOriginalName();
            }

            if ($data['password'] && $data['password'] == $data['password-c']) {
                $data['password'] = bcrypt($data['password']);
            }
            else {
                $data['password'] = $user->password;
            }

            if ($user->update($data)) {
                if (!empty($file)) {
                    $file->move('upload/user/avatar/', $file->getClientOriginalName());
                }
                return redirect('/account/profile')->with('success', __('Update profile success.'));
            }
            else {
                return redirect()->back()->withErrors('Update profile fail');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
