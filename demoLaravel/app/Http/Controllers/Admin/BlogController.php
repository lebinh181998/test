<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\User;
use App\Http\Requests\admin\CreateBlogRequest;
use App\Http\Requests\admin\UpdateBlogRequest;
use Auth;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function list()
    {
        $stt = 1;
        $getBlog = Blog::paginate(3);
        return view('admin.blog.blog', compact('getBlog', 'stt'));
    }

    public function delete($id)
    {
        $blog = Blog::find($id);
        $blog->delete();
        $stt = 1;
        $getBlog = Blog::paginate(3);
        return redirect('/admin/blog')->with('success', __('Delete blog success.'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/blog/createblog');
    }

    public function create_success(CreateBlogRequest $request)
    {
        $data = $request->all();
        $blog = new Blog();
        $userId = Auth::id();
        $user = User::findOrFail($userId)->toArray();
        $file = $data['image'];

        $blog->title = $data['title'];
        if (!empty($file)) {
             $blog->image = $file->getClientOriginalName();
        }
        $blog->description = $data['description'];
        $blog->content = $data['content'];
        $blog->user_name = $user['name'];

        if ($blog->save()) {
            if (!empty($file)) {
                $file->move('upload/user/avatar/', $file->getClientOriginalName());
            }
            return redirect('/admin/blog')->with('success', __('Create blog success.'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getBlog = Blog::find($id)->toArray();   
        return view('admin.blog.updateblog', compact('getBlog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBlogRequest $request, $id)
    {
        $data = $request->all();
        $blog = Blog::findOrFail($id);
        $file = $request->image;
 
        if (!empty($file)) {
             $data['image'] = $file->getClientOriginalName();
        }

        if ($blog->update($data)) {
            if (!empty($file)) {
                $file->move('upload/user/avatar/', $file->getClientOriginalName());
            }
            return redirect('/admin/blog')->with('success', __('Update blog success.'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
