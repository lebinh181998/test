<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\API\InsertProductRequest;
use App\Models\Product;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Sale;

use Intervention\Image\Facades\Image as Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $getDBProducts = Product::paginate(9);
        foreach ($getDBProducts as  $product) {
            $getImage[$product['id']] = json_decode($product['image']);
        }

        return response()->json([
            'status' => 200,
            'response' => 'success',
            'getAllProduct' => $getDBProducts,
            'getImageProducts' => $getImage,
        ]);
    }

    public function uploadFile($file, $arrImage, $userId)
    {
        foreach ($file as $key => $image) {
            $name = $arrImage[$key];
            $name2 = '2'.$arrImage[$key];
            $name3 = '3'.$arrImage[$key];
            $path = public_path('upload/product/'.$userId.'/'.$name);
            $path2 = public_path('upload/product/'.$userId.'/'.$name2);
            $path3 = public_path('upload/product/'.$userId.'/'.$name3);
            Image::make($image->getRealPath())->resize(329,380)->save($path);
            Image::make($image->getRealPath())->resize(85,85)->save($path2);
            Image::make($image->getRealPath())->resize(350,400)->save($path3);
        }
    }

    public function addForm()
    {
        $getCategory = Category::All()->toArray();
        $getBrand = Brand::All()->toArray();
        $getSale = Sale::All()->toArray();
        return response()->json([
            'status' => 200,
            'response' => 'error',
            'category' => $getCategory,
            'brand' => $getBrand,
            'sale' => $getSale,
        ]);
    }

    public function add(InsertProductRequest $request)
    {
        $data = $request->all();
        foreach ($request->image as $image) {
            $name = strtotime(date('Y-m-d H:i:s'))."_".$image;
            $arrImage[] =$name;
        }
        $data['image'] = json_encode($arrImage);

        if (Product::create($data)) {
            if (!file_exists('upload/product/'.$data['user_id'])) {
                 mkdir('upload/product/'.$data['user_id']);
            }
            $this->uploadFile($request->file('imageRQ'), $arrImage, $data['user_id']);
            return response()->json([
                'status' => 200,
                'response' => 'success',
            ]);
        }
        else {
            return response()->json([
                'status' => 401,
                'response' => 'error',
                'error' => 'error server',
            ]);
        }
    }

    public function viewCart(Request $request)
    {
        $getSSProduct = json_decode($request->products);

        if (!empty($getSSProduct)) {
            foreach ($getSSProduct as $key => $value) {
                $arrProductID[] = $key;
                $arrProductQTY[$key] = $value;
            }
            $getProducts = Product::whereIn('id', $arrProductID)->get();

            foreach ($getProducts as  $product) {
                $getImages[$product['id']] = json_decode($product['image']);
                // $sum += $arrProductQTY[$product['id']] * $product['price'];
            }
        }

        return response()->json([
            'status' => 200,
            'response' => 'success',
            'getProducts' => $getProducts,
            'getImages' => $getImages,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function editForm($id)
    {
        if ($id) {
            $getCategoryDB = Category::All()->toArray();
            $getBrandDB = Brand::All()->toArray();
            $getSaleDB = Sale::All()->toArray();
            $getProductDB = Product::find($id)->toArray();
            $getImage = json_decode($getProductDB['image']);

            return response()->json([
                'status' => 200,
                'response' => 'success',
                $getCategoryDB,
                $getBrandDB,
                $getSaleDB,
                $getProductDB,
                $getImage,
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data = $request->all();
        $product = Product::findOrFail($id);
        if ($product->count() > 0) {
            foreach ($request->image as $image) {
                $name = strtotime(date('Y-m-d H:i:s'))."_".$image;
                $arrImage[] =$name;
            }
            $data['image'] = json_encode($arrImage);

            if ($product->update($data)) {
                if (!file_exists('upload/product/'.$data['user_id'])) {
                     mkdir('upload/product/'.$data['user_id']);
                }
                $this->uploadFile($request->file('imageRQ'), $arrImage, $data['user_id']);
                return response()->json([
                    'status' => 200,
                    'response' => 'success',
                ]);
            }
            else {
                return response()->json([
                    'status' => 401,
                    'response' => 'error',
                    'error' => 'error server',
                ]);
            }
        }
    }

    public function delete($id)
    {
        if ($id) {
            $product = Product::find($id);
            $product->delete();
            return response()->json([
                'status' => 200,
                'response' => 'success',
                $id,
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
