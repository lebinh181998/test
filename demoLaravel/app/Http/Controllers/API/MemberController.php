<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\API\RegisterRequest;
use App\Http\Requests\API\LoginRequest;
use App\Models\User;
use App\Models\Country;
use App\Models\Product;
use App\Http\Requests\API\UpdateProfileRequest;
use Auth;

use Intervention\Image\Facades\Image as Image;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function register(RegisterRequest $request)
    {
        $data = $request->all();
        $file = $request->get('avatar');

        if($file) {
           $image = $file;
           $name = time(). '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
           $data['avatar'] = $name;
        }

        $data['password'] = bcrypt($data['password']);

        if ($getUser = User::create($data)) {
            if($file){
                Image::make($file)->save(public_path('upload/user/avatar/').$data['avatar']);
            }
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $user = Auth::user(); 
                $success['token'] =  $user->createToken('MyApp')->accessToken;

                return response()->json([
                    'status' => 200,
                    'response' => 'success',
                    'success' => $success,
                    'Auth' => Auth::user(),
                    $getUser,
                ]);
            }
        }
        else {
            return response()->json([
                'status' => 422,
                'response' => 'error',
                'error' => 'error sever',
            ]);
        }
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            if (Auth::user()->level == 0) {
                $user = Auth::user(); 
                $success['token'] =  $user->createToken('MyApp')->accessToken;

                return response()->json([
                    'status' => 200,
                    'response' => 'success',
                    'success' => $success,
                    'Auth' => Auth::user(),
                ]);
            }
            else {
                // Auth::logout();
                $error = 'Tài khoản hoặc mật khẩu sai';
                return response()->json([
                    'status' => 200,
                    'error' => $error,
                    'response' => 'error',
                ]);
            }
        }
        else {
            $error = 'Tài khoản hoặc mật khẩu sai';
            return response()->json([
                'status' => 200,
                'error' => $error,
                'response' => 'error',
            ]);
        }
    }

    public function updateProfile(UpdateProfileRequest $request)
    {
        $data = $request->all();
        $user = User::findOrFail($data['user_id']);
        $file = $request->avatar;

        if($file) {
           $image = $file;
           $name = time(). '.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
           $data['avatar'] = $name;
        }
        else {
            unset($data['avatar']);
        }

        if ($data['password']) {
            $data['password'] = bcrypt($data['password']);
        }
        else {
            unset($data['password']);
        }

        if ($user->update($data)) {
            if($file){
                Image::make($file)->save(public_path('upload/user/avatar/').$data['avatar']);
            }
            return response()->json([
                'status' => 200,
                'response' => 'success',
                'Auth' => $user,
            ]);
        }
        else {
            return response()->json([
                'status' => 200,
                'response' => 'error',
            ]);
        }
        
    }

    public function listMyProduct($id)
    {
        $getListProduct = Product::where('user_id', $id)->paginate(10);

        foreach ($getListProduct as $product) {
            $getImage[$product->id] = json_decode($product['image']);
        }

        return response()->json([
            'status' => 200,
            'response' => 'success',
            'listMyProduct' => $getListProduct,
            'image' => $getImage,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
