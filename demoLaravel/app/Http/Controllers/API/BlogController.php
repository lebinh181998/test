<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Comment;
use App\Models\Rate;


class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    public function list()
    {
        $getDBBlog = Blog::paginate(2);

        return response()->json([
            'status' => 200,
            'blog' => $getDBBlog,
        ]);
    }

    public function detail($id)
    {
        $getDBBlog = Blog::with('comment')->find($id);

        return response()->json([
            'status' => 200,
            'detail' => $getDBBlog,
        ]);
    }

    public function addComment(Request $request)
    {
        $data = $request->all();
        $comment = new Comment();

        $comment->avatar = $data['avatar'];
        $comment->user_name = $data['user_name'];
        $comment->comment = $data['comment'];
        $comment->comment_id = $data['comment_id'];
        $comment->user_id = $data['user_id'];
        $comment->blog_id = $data['blog_id'];

        if($comment->save()) {
            return response()->json([  
                'status' => 200,
                'response' => 'success',
                'data' => $request->all(),
            ]);
        }
        else {
            return response()->json([
                'status' => 200,
                'response' => 'error',
            ]);
        }
    }

    public function rate($id)
    {
        $rate = Rate::where('blog_id', $id)->get();
        $sumrate = 0;

        if ($rate->count() > 0) {
            foreach ($rate as $value) {
                $sumrate += $value['rate'];
            }
            $average = round($sumrate/count($rate));
        }
        else {
            $average = 0;
        }

        return response()->json([
            'status' => 200,
            'response' => 'success',
            'rate' => $average,
        ]);
    }

    public function addRate(Request $request)
    {
        $data = $request->all();
        $rate_blog = Rate::where('blog_id', $data['blog_id'])->get();
        $rate_user = Rate::where([ ['blog_id', $data['blog_id']], ['user_id', $data['user_id']] ])->get();
        $flag = false;

        $rate = new Rate();
        $rate->rate = $data['rate'];
        $rate->user_id = $data['user_id'];
        $rate->blog_id = $data['blog_id'];

        if ($rate_blog->count() <= 0) {
            $flag = true;
        }
        else {
            if($rate_user->count() > 0) {
                return response()->json([
                    'status' => 200,
                    'response' => 'fail',
                    'fail' => 'Bạn đã đánh giá bài viết này rồi',
                ]);
            }
            else {
                $flag = true;
            }
        }

        if ($flag == true) {
            if($rate->save()) {
                $sumrate = 0;
                foreach ($rate_blog as $value) {
                    $sumrate += $value['rate'];
                }
                $average = round( ($sumrate + $data['rate'])/(count($rate_blog) + 1) );

                return response()->json([
                    'status' => 200,
                    'response' => 'success',
                    'success' => $average,
                ]);
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
