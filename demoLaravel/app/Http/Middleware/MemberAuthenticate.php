<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class MemberAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->level == 1 ){
            Auth::logout();
            return redirect('/member/login');
        }
        return $next($request);
    }
}
