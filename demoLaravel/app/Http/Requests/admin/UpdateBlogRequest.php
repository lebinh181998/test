<?php

namespace App\Http\Requests\admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'image|mimes:jpg,png,gif,JPG,PNG,GIF',
        ];
    }

    public function messages()
    {
        return [
            'image' => ':attribute không phải file ảnh',
            'mimes' => ':attribute phải có đuôi :values',
        ];
    }
}
