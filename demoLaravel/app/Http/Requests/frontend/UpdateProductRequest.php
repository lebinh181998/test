<?php

namespace App\Http\Requests\frontend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'sale' => 'max:100',
            'price' => 'required|numeric',
            'image' => 'image|mimes:jpeg,png,PNG,jpg,gif|max:2048',
            'detail' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Vui lòng nhập :attribute',
            'max' => ':attribute không thể quá :max',
            'numeric' => ':attribute chỉ được nhập số',
            'image' => ':attribute này không phải ảnh',
            'mimes' => ':attribute phải có đuôi :values',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'tên sản phẩm',
            'sale' => 'giảm giá',
            'price' => 'giá sản phẩm',
            'image' => 'file',
            'detail' => 'chi tiết sản phẩm',
        ];
    }
}
