<?php

namespace App\Http\Requests\frontend;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|max:191',
            'email' =>'required|email|unique:users,email',
            'password'=>'required',
            'password-c' =>'same:password',
            'phone' => 'numeric',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,JPEG,PNG,JPG,GIF|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'required' =>'Vui lòng nhập :attribute',
            'email' =>':attribute không nhập đúng định dạng',
            'unique' =>':attribute đã tồn tại',
            'same' =>'Vui lòng :attribute',
            'numeric' =>':attribute phải là số',
            'image' =>':attribute không phải là file ảnh',
            'mimes' =>':attribute phải có đuôi :values',
            'max' =>':attribute không được lớn hơn :max',
        ];
    }

     public function attributes()
    {
        return [
            'name'=>'Username',
            'email' =>'Email',
            'password'=>'Password',
            'password-c' =>'Confirm Password',
            'phone' => 'Phone',
            'avatar' => 'Avatar',
        ];
    }
}
