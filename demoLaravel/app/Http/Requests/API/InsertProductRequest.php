<?php

namespace App\Http\Requests\API;

use App\Http\Requests\API\FormRequest;

class InsertProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'number' => 'max:100',
            'price' => 'required|numeric',
            'imageRQ' => 'array|max:3',
            'detail' => 'required',
            'imageRQ.*' => 'image|mimes:jpeg,png,jpg,gif,JPEG,PNG,JPG,GIF|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Vui lòng nhập :attribute',
            'max' => ':attribute không thể quá :max',
            'numeric' => 'Chỉ được nhập số cho :attribute',
            'image' => ':attribute này không phải ảnh',
            'mimes' => ':attribute phải có đuôi :values',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'tên sản phẩm',
            'category' => 'thể loại',
            'brand' => 'nhãn hàng',
            'sale' => 'giảm giá',
            'price' => 'giá sản phẩm',
            'imageRQ' => 'file',
            'detail' => 'chi tiết sản phẩm',
        ];
    }
}
