<?php

namespace App\Http\Requests\API;

use App\Http\Requests\API\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|max:191',
            'password'=>'same:conf_password',
            'image' => 'image|mimes:jpeg,png,PNG,jpg,gif|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'required'=>':attribute không được để trống',
            'same'=>':attribute không trùng với password confirm',
            'image' => ':attribute này không phải là ảnh',
            'mimes' => ':attribute này phải có đuôi :values',
            'max' => ':attribute có size quá lớn',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Username',
            'password'=> 'Password',
            'image' => 'File',
        ];
    }
}
