<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    protected $fillable = [
        'name', 'price','category_id', 'brand_id', 'status', 'sale', 'tag', 'image', 'detail', 'user_id',
    ];
}
